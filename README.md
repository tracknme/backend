## Sobre o sistema
O sistema deve ser pensado para gerenciar os funcionários de uma empresa pequena. 

- Deve permitir cadastrar, buscar, atualizar e deletar um funcionário, tudo utilizando padrão REST.
- É permitido utilizar qualquer dependência adicional.
- O código do projeto deve estar disponível em um repositório publico GIT de sua escolha.
- O prazo para a conclusão do desafio é de **uma semana**.

### As etapas extras são opcionais e não é necessário fazer todas caso opte em fazer alguma.

## 1ª Etapa
Criar um projeto utilizando:

- Java 8+
- Spring Boot
- Spring Data
- Utilizar uma arquitetura clara na organização do projeto

## 2ª Etapa
O funcionário, deve conter os seguintes atributos: 

- id
- nome **Obrigatorio**
- idade **Obrigatorio**
- cep **Obrigatorio**
- sexo
- endereço
- bairro
- cidade
- estado

## 3ª Etapa
Criar os seguintes endpoints.

Todos os endpoints devem receber e responder no formato JSON. 

- POST - Cadastrar um funcionário
- GET - Buscar um funcionário específico pelo id dele
- GET - Buscar todos os funcionários cadastrados
- GET - Buscar funcionários por CEP
- PUT - Atualizar a entidade funcionário 
- DELETE - Excluir funcionário

## 4ª Etapa
Criar testes para validar todos os métodos e classes.

------------
### As etapas extras são opcionais e não é necessário fazer todas caso opte em fazer alguma.

## 5ª Etapa (Extra)
Quando informado apenas o CEP na criação do funcionário, o sistema deve buscar o endereço e setar os atributos correspondentes.

Utilizar a seguinte API **https://viacep.com.br/** para consulta de CEP.

## 6ª Etapa (Extra)
Adicionar um endpoint utilizando o método **PATCH** para atualizar qualquer atributo do funcionario sem a necessidade de passar o funcionario inteiro.

**Lembrando que se o CEP for alterado o mesmo deve consultar o endereço novamente.**

## 7ª Etapa (Extra)
Implemente cache na chamada do ViaCep e em seus endpoints GET's, caso algum funcionario tenha um valor alterado ou deletado o cache daquela chave deve ser atualizado ou excluido conforme a necessidade.

## 8ª Etapa (Extra)
Crie um gateway gRPC para as funções:

- Cadastrar um funcionario
- Buscar um funcionario específico pelo id dele
- Buscar todos os funcionario cadastrados
- Buscar funcionario por CEP

**O arquivo .proto deve ficar na raiz do projeto**

## 9ª Etapa (Extra)
Use Hexagonal Architecture ou Clean Architecture no projeto.

## 10ª Etapa (Extra)
Subir o seu projeto em uma CLOUD de sua escolha.

## 11ª Etapa (Extra)
Configure uma CI/CD para compilar e fazer um deploy na CLOUD sempre que houver alteração na branch master.